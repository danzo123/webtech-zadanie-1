<?php
	 function getData(){
		$data = array();
		array_push($data, array(
				"rok" => "2012/13",
				"celkovo" => "62",
				"A" => "20",
				"B" => "11",
				"C" => "13",
				"D" => "7",
				"E" => "5",
				"FX" => "0",
				"FN" => "1",
		));

		array_push($data, array(
				"rok" => "2013/14",
				"celkovo" => "53",
				"A" => "20",
				"B" => "19",
				"C" => "6",
				"D" => "3",
				"E" => "1",
				"FX" => "0",
				"FN" => "0",
		));

		array_push($data, array(
				"rok" => "2014/15",
				"celkovo" => "53",
				"A" => "9",
				"B" => "19",
				"C" => "22",
				"D" => "0",
				"E" => "0",
				"FX" => "0",
				"FN" => "3",
		));

		return $data;
    }

	$font = "../../www/fonts/arial.ttf";

	$height = 200;
	$obrazok = imagecreatetruecolor( 1100, $height+100 );
	$type = $_GET["type"];
	
	/*
		farby
	*/
	$black = imagecolorallocate($obrazok, 0x00, 0x00, 0x00);
	$red =   imagecolorallocate($obrazok, 0xFF, 0x00, 0x00);
	$green = imagecolorallocate($obrazok, 0x00, 0xFF, 0x00);
	$blue =  imagecolorallocate($obrazok, 71, 71, 195);
	$white = imagecolorallocate($obrazok, 0xFF, 0xFF, 0xFF);
	$yellow = imagecolorallocate($obrazok, 0xFF, 0xFF, 0x00);
	$purple = imagecolorallocate($obrazok, 218, 112, 214);
	$grey = imagecolorallocate($obrazok, 139, 139, 139);
	$nevim = imagecolorallocate($obrazok, 255, 230, 150);

	$colors = array(
		"A" => $green,
		"B" => $blue,
		"C" => $yellow,
		"D" => $red,
		"E" => $purple,
		"FN" => $grey,
		"FX" => $nevim,
		"NZ" => $black
	);

	imagefilledrectangle($obrazok, 0, 0, 1100, $height+100, $white);
	
	$data = getData();
	$x = 50;

	foreach ($data as $row) {

		imagettftext($obrazok,12, 0, $x, 17, $black, $font, "Rok: " . $row["rok"]); 
		imagettftext($obrazok,12, 0, $x, 34, $black, $font, "Celkovo žiakov: " . $row["celkovo"]); 

		if ($type == "pie"){
			
			$celkovo = $row["celkovo"];
			$bez_zapoctu = $celkovo - ($row["A"] + $row["B"] + $row["C"] + $row["D"] + $row["E"] + $row["FX"] + $row["FN"]);

			unset($row["rok"]);
			unset($row["celkovo"]);
			$uhol = 0;

			foreach ($row as $key => $value) {
				if ($value > 0){
					imagefilledarc($obrazok, $x+100, 150, 200, 200, $uhol, $uhol + ($value/$celkovo)*360, $colors[$key], IMG_ARC_PIE);
					$uhol +=  $value/$celkovo*360;
				}
			}
			if ($bez_zapoctu > 0)
				imagefilledarc($obrazok, $x+100, 150, 200, 200, $uhol, $uhol + ($bez_zapoctu/$celkovo)*360, $colors["NZ"], IMG_ARC_PIE);

			/*
				legenda
			*/
			$font_size = 8;
			$y = 0;

			

			foreach ($row as $key => $value) {
				$body_legenda = array(
					$x+210, $y,
					$x+290, $y,
					$x+290, $y + 20,
					$x+210, $y + 20
				);

				imagefilledpolygon($obrazok, $body_legenda, 4, $colors[$key]);
				imagettftext($obrazok, $font_size, 0, $x+220, $y+15, $black, $font, $key . " (" . round(($value/$celkovo)*10000)/100 . "%)");
				$y += 22;
			}

			$body_legenda = array(
				$x+210, $y,
				$x+290, $y,
				$x+290, $y + 20,
				$x+210, $y + 20
			);

			imagefilledpolygon($obrazok, $body_legenda, 4, $colors["NZ"]);
			imagettftext($obrazok, $font_size, 0, $x+220, $y+15, $white, $font,  "NZ (" . round(($bez_zapoctu/$celkovo)*10000)/100 . "%)");
			$y += 22;

		}elseif ($type == "column"){
			
			$uspech = $row["A"] + $row["B"] + $row["C"] + $row["D"] + $row["E"];
			$neuspech = $row["celkovo"] - $uspech;
			
			/*
				osi
			*/

			// x-ova os, text
			imageline($obrazok, $x-50, $height + 1, $x+280, $height + 1, $black);
			imagettftext($obrazok, 10, 0, $x+200, $height + 1, $black, $font, "úspešnosť");

			// y-ova os, text
			imageline($obrazok, $x-20, 50, $x-20, $height+25, $black);
			imagettftext($obrazok, 10, 90, $x-21, 120, $black, $font, "počet žiakov");

			/*
				obdlznik vyjadrujuci uspech + text
			*/
			$body = array(
				$x, $height,
				$x+50, $height,
				$x+50, $height - 2*$uspech, 
				$x, $height - 2*$uspech,
			);

			imagefilledpolygon($obrazok, $body, 4, $green);
			imagettftext($obrazok,11, 0, $x, $height+17, $black, $font, "úspech \n  ($uspech)");

			/*
				obdlznik vyjadrujuci neuspech + text
			*/
			$body = array(
				$x+100, $height,
				$x+165, $height,
				$x+165, $height - 2*$neuspech, 
				$x+100, $height - 2*$neuspech,
			);

			imagefilledpolygon($obrazok, $body, 4, $red);
			imagettftext($obrazok,11, 0, $x+100, $height+17, $black, $font, "neúspech \n     ($neuspech)");
		}
		$x += 350;
	}
	/*
		vykreslenie obrazka + uvolnenie pamete
	*/
	header("Content-type: image/png");

	imagepng($obrazok);

	imagedestroy($obrazok);

?>