<?php
function getData(){
		$data = array();
		array_push($data, array(
				"rok" => "2012/13",
				"celkovo" => "62",
				"A" => "20",
				"B" => "11",
				"C" => "13",
				"D" => "7",
				"E" => "5",
				"FX" => "0",
				"FN" => "1",
		));

		array_push($data, array(
				"rok" => "2013/14",
				"celkovo" => "53",
				"A" => "20",
				"B" => "19",
				"C" => "6",
				"D" => "3",
				"E" => "1",
				"FX" => "0",
				"FN" => "0",
		));

		array_push($data, array(
				"rok" => "2014/15",
				"celkovo" => "53",
				"A" => "9",
				"B" => "19",
				"C" => "22",
				"D" => "0",
				"E" => "0",
				"FX" => "0",
				"FN" => "3",
		));

		return $data;
	}
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
	google.charts.load('current', {'packages':['corechart']});
</script>

<div class="row">
	<?php
		$data = getData();

		foreach ($data as $key => $d) {
	?>
		<div class="col-md-4" id="column_div<?php echo $key ?>"></div>
		<script>
		 	
		 	google.charts.setOnLoadCallback(drawColumn<?php echo $key ?>);

		 	function drawColumn<?php echo $key ?>() {

				var data = google.visualization.arrayToDataTable([
					['Úspešnosť', 'Počet žiakov', { role: 'style' } ],
					['Úspešne', <?php echo $d["A"] + $d["B"] + $d["C"] + $d["D"] + $d["E"];?>, 'color: gray'],
					['Neuspesne', <?php echo $d["celkovo"] - ($d["A"] + $d["B"] + $d["C"] + $d["D"] + $d["E"]) ?>, 'color: #76A7FA'],
				]);

				var options = {'title':'Rok: <?php echo $d["rok"] ?> \n Celkovo žiakov: <?php echo $d["celkovo"] ?>',
			                       'width':400,
			                       'height':300,
			                   		legend: {position: 'none'}};

				var chart = new google.visualization.ColumnChart(document.getElementById('column_div<?php echo $key ?>'));
				chart.draw(data, options);
			}
		</script>
	<?php
		}
	?>
</div>


<div class="row">
	<?php
		$data = getData();

		foreach ($data as $key => $d) {
	?>
		<div class="col-md-4" id="chart_div<?php echo $key ?>"></div>
		<script>
		 	
		 	google.charts.setOnLoadCallback(drawChart<?php echo $key ?>);

		 	function drawChart<?php echo $key ?>() {

		        var data = new google.visualization.DataTable();
		        data.addColumn('string', 'Znamka');
		        data.addColumn('number', 'Percento');
		        
		        var options = {'title':'Rok: <?php echo $d["rok"] ?> \n Celkovo žiakov: <?php echo $d["celkovo"] ?>',
		                       'width':400,
		                       'height':300};

		        data.addRows([
		        	['NZ', <?php echo $d["celkovo"] - ($d["A"] + $d["B"] + $d["C"] + $d["D"] + $d["E"] + $d["FX"] + $d["FN"]); ?>],
		        	<?php 
		        		unset($d["rok"]);
		        		unset($d["celkovo"]);
		        		foreach ($d as $keyy => $value) {
		        			echo "['$keyy', $value],";
		          		}
		          	?>

		        ]);

		        var chart = new google.visualization.PieChart(document.getElementById('chart_div<?php echo $key ?>'));
		        chart.draw(data, options);
		      }
		</script>
	<?php
		}
	?>
</div>


