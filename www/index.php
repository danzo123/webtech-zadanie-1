<?php
	$basePath = "http://" . $_SERVER["HTTP_HOST"] . "/zadanie1/www";

	$requested_file = isset($_GET["file"]) ? $_GET["file"] : null;

	if ( $requested_file && $requested_file != "index" && $requested_file != "homepage")
		if (file_exists('../app/pages/' . $requested_file . ".php")){
			$file = $requested_file;
			$path = '../app/pages/' . $requested_file . ".php";
		}else{
			$file = "404";
			$path = '../app/pages/404.html';
		}
	else{
		if ($requested_file)
			header('Location: ' . $basePath);
		$file = "homepage";
		$path = '../app/pages/homepage.php';
	}

	// nacitanie <title> zo suboru
	$tmp = explode("\n",file_get_contents($basePath."/../app/config/titles.neon"));
	$titles = array();
	foreach ($tmp as $t) {
		$expl = explode(":", $t);
		$titles[trim($expl[0])] = trim($expl[1]);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title><?php echo isset($titles[$file])?$titles[$file]:$file; ?> | Zadanie 1</title>

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css">

		<script type="text/javascript" src="<?php echo $basePath?>/js/jquery-2.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo $basePath?>/js/bootstrap.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-inverse">
			<div class="container-fluid"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button> 
					<a class="navbar-brand" href="<?php echo $basePath?>">Zadanie 1</a> 
				</div> 

				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-6"> 
					<ul class="nav navbar-nav"> 
						<li <?php if ($file == "homepage") echo "class='active'";?>>
							<a href="<?php echo $basePath?>">Domov</a>
						</li>
						<li <?php if ($file == "uloha1") echo "class='active'";?> >
							<a href="<?php echo $basePath . '?file=uloha1'; ?>">Úloha 1</a>
						</li>
						<li <?php if ($file == "uloha2") echo "class='active'";?>>
							<a href="<?php echo $basePath . '?file=uloha2'; ?>">Úloha 2</a>
						</li> 
						<li <?php if ($file == "uloha3") echo "class='active'";?>>
							<a href="<?php echo $basePath . '?file=uloha3'; ?>">Úloha 3</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
		<?php if (isset($_GET["file"]) && $file != "404" ){ ?>
			<table class="table" >
				<thead>
					<tr>
						<th>šk. rok</th>
						<th>počet študentov</th>
						<th>A</th>
						<th>B</th>
						<th>C</th>
						<th>D</th>
						<th>E</th>
						<th>FX</th>
						<th>FN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2012/13</td>
						<td>62</td>
						<td>20</td>
						<td>11</td>
						<td>13</td>
						<td>7</td>
						<td>5</td>
						<td>0</td>
						<td>1</td>
					</tr>
					<tr>
						<td>2013/14</td>
						<td>53</td>
						<td>20</td>
						<td>19</td>
						<td>6</td>
						<td>3</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>2014/15</td>
						<td>53</td>
						<td>9</td>
						<td>19</td>
						<td>22</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>3</td>
					</tr>
				</tbody>
			</table>
			
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
		<?php } ?>
		<?php
			require_once($path);
		?>
		</div>
	</body>
</html>