<div class="row">
	<div class="col-md-12">
		<h1>
			Zadanie 1
		</h1>
	</div>
</div>
<?php
	function createThumb($url, $thumb_width, $thumb_height){
		$name = md5($url);
		$path = __DIR__ . "/../../www/images/thumb/".$thumb_width."x".$thumb_height."/";
		$img = imagecreatefrompng ($url);

		if (!file_exists($path."/$name")){
			if (!file_exists($path)){
				if (!@mkdir($path, 0755, true)){
					$error = error_get_last();
					echo $error['message'];
					exit;
				}
			}

			$width = imagesx( $img );
			$height = imagesy( $img );

			$new_width = $thumb_width;
			$new_height = floor( $height * ( $thumb_width / $width ) );

			$tmp_img = imagecreatetruecolor( $new_width, $new_height );

			imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			imagepng($tmp_img, $path."/$name", 0);
		}

		return "images/thumb/".$thumb_width."x".$thumb_height."/$name";
	}
?>

<div class="row" id="homepage">
	<div class="col-md-4 ">
		<div class="col-xs-12" style="height: 200px; background-image: url('<?php echo createThumb($basePath."/../app/libraries/gdgraph.php?type=column", 400,200); ?>'); background-size: 100% 100%; background-repeat: no-repeat;">
			<div class="overlay" id="overlay1" data-href="<?php echo $basePath ?>/?file=uloha1">
				<h3>Úloha 1</h3>
			</div>
		</div>
	</div>

	<div class="col-md-4 ">
		<div class="col-xs-12" style="height: 200px; background-image: url('<?php echo createThumb($basePath."/../app/libraries/gdgraph.php?type=pie", 400,200); ?>'); background-size: 100% 100%; background-repeat: no-repeat;">
			<div class="overlay" id="overlay2" data-href="<?php echo $basePath ?>/?file=uloha2">
				<h3>Úloha 2</h3>
			</div>
		</div>
	</div>

	<div class="col-md-4 ">
		<div class="col-xs-12" style="height: 200px; background-image: url('<?php echo createThumb($basePath."/images/uloha3.png", 400,200); ?>'); background-size: 100% 100%; background-repeat: no-repeat;">
			<div class="overlay" id="overlay3" data-href="<?php echo $basePath ?>/?file=uloha3">
				<h3>Úloha 3</h3>
			</div>
		</div>
	</div>
	
	<script>
		$(document).ready(function(){
			$("#homepage .col-md-4 .col-xs-12").hover(function(){
				$(this).children(".overlay").show();
			},function(){
				$(this).children(".overlay").hide();
			});

			$(".overlay").click(function(){
				window.location.href = $(this).data("href");
			});
		});
	</script>
</div>